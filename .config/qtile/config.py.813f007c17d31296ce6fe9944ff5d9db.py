import os
import os
import re
import socket
import subprocess
from libqtile.config import Drag, Key, Screen, Group, Drag, Click, Rule
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.widget import Spacer

# import arcobattery

# mod4 or mod = super key
mod = "mod4"
mod1 = "alt"
mod2 = "control"
home = os.path.expanduser("~")


@lazy.function
def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)


@lazy.function
def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)


keys = [
    # FUNCTION KEYS
    Key([], "F12", lazy.spawn("xfce4-terminal --drop-down")),
    # SUPER + FUNCTION KEYS
    # General behaviour keybindings
    Key([mod], "f", lazy.window.toggle_fullscreen()),
    Key([mod], "q", lazy.window.kill()),
    Key([mod], "Escape", lazy.spawn("xkill")),
    Key([mod], "x", lazy.spawn("arcolinux-logout")),
    # Launch Program
    Key([mod], "Return", lazy.spawn("termite")),
    Key([mod], "c", lazy.spawn("code")),
    Key([mod], "i", lazy.spawn("insomnia")),
    Key([mod], "t", lazy.spawn("tableplus")),
    Key([mod], "v", lazy.spawn("pavucontrol")),
    Key([mod], "m", lazy.spawn("thunderbird")),
    Key([mod], "w", lazy.spawn("firefox")),
    Key([mod], "g", lazy.spawn("gimp")),
    Key([mod], "p", lazy.spawn("pcmanfm")),
    Key([mod], "d", lazy.spawn("cdw")),
    # SUPER + SHIFT KEYS
    Key([mod, "shift"], "Return", lazy.spawn("dmenu_run -i")),
    Key([mod, "shift"], "q", lazy.window.kill()),
    Key([mod, "shift"], "r", lazy.restart()),
    Key([mod, "control"], "r", lazy.restart()),
    # Key([mod, "shift"], "x", lazy.shutdown()),
    Key(["mod1", "control"], "a", lazy.spawn("xfce4-appfinder")),
    Key(["mod1", "control"], "c", lazy.spawn("catfish")),
    Key(["mod1", "control"], "e", lazy.spawn("arcolinux-tweak-tool")),
    Key(["mod1", "control"], "f", lazy.spawn("firefox")),
    Key(["mod1", "control"], "g", lazy.spawn("chromium -no-default-browser-check")),
    Key(["mod1", "control"], "i", lazy.spawn("nitrogen")),
    Key(["mod1", "control"], "k", lazy.spawn("arcolinux-logout")),
    Key(["mod1", "control"], "l", lazy.spawn("arcolinux-logout")),
    Key(["mod1", "control"], "m", lazy.spawn("xfce4-settings-manager")),
    Key(
        ["mod1", "control"],
        "o",
        lazy.spawn(home + "/.config/qtile/scripts/picom-toggle.sh"),
    ),
    Key(["mod1", "control"], "p", lazy.spawn("pamac-manager")),
    Key(["mod1", "control"], "r", lazy.spawn("rofi-theme-selector")),
    Key(["mod1", "control"], "s", lazy.spawn("spotify")),
    Key(["mod1", "control"], "t", lazy.spawn("termite")),
    Key(["mod1", "control"], "u", lazy.spawn("pavucontrol")),
    Key(["mod1", "control"], "v", lazy.spawn("vivaldi-stable")),
    Key(["mod1", "control"], "w", lazy.spawn("arcolinux-welcome-app")),
    Key(["mod1", "control"], "Return", lazy.spawn("termite")),
    # ALT + ... KEYS
    Key(["mod1"], "f", lazy.spawn("variety -f")),
    # Key(["mod1"], "h", lazy.spawn('urxvt -e htop')),
    Key(["mod1"], "n", lazy.spawn("variety -n")),
    Key(["mod1"], "p", lazy.spawn("variety -p")),
    Key(["mod1"], "t", lazy.spawn("variety -t")),
    Key(["mod1"], "Up", lazy.spawn("variety --pause")),
    Key(["mod1"], "Down", lazy.spawn("variety --resume")),
    Key(["mod1"], "Left", lazy.spawn("variety -p")),
    Key(["mod1"], "Right", lazy.spawn("variety -n")),
    Key(["mod1"], "F2", lazy.spawn("gmrun")),
    Key(["mod1"], "F3", lazy.spawn("xfce4-appfinder")),
    # VARIETY KEYS WITH PYWAL
    Key(
        ["mod1", "shift"],
        "f",
        lazy.spawn(home + "/.config/qtile/scripts/set-pywal.sh -f"),
    ),
    Key(
        ["mod1", "shift"],
        "p",
        lazy.spawn(home + "/.config/qtile/scripts/set-pywal.sh -p"),
    ),
    Key(
        ["mod1", "shift"],
        "n",
        lazy.spawn(home + "/.config/qtile/scripts/set-pywal.sh -n"),
    ),
    Key(
        ["mod1", "shift"],
        "u",
        lazy.spawn(home + "/.config/qtile/scripts/set-pywal.sh -u"),
    ),
    # CONTROL + SHIFT KEYS
    Key([mod2, "shift"], "Escape", lazy.spawn("xfce4-taskmanager")),
    # SCREENSHOTS
    Key(
        [],
        "Print",
        lazy.spawn(
            "scrot 'ArcoLinux-%Y-%m-%d-%s_screenshot_$wx$h.jpg' -e 'mv $f $$(xdg-user-dir PICTURES)'"
        ),
    ),
    Key([], "Print", lazy.spawn("xfce4-screenshooter")),
    # MULTIMEDIA KEYS
    # ThinkVantage Key
    Key([], "XF86Launch1", lazy.spawn("arcolinux-logout")),
    # INCREASE/DECREASE BRIGHTNESS
    Key([], "XF86MonBrightnessUp", lazy.spawn("xbacklight -inc 5")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("xbacklight -dec 5")),
    # INCREASE/DECREASE/MUTE VOLUME
    Key([], "XF86AudioMute", lazy.spawn("amixer -q set Master toggle")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -q set Master 5%-")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -q set Master 5%+")),
    Key([], "XF86AudioPlay", lazy.spawn("playerctl play-pause")),
    Key([], "XF86AudioNext", lazy.spawn("playerctl next")),
    Key([], "XF86AudioPrev", lazy.spawn("playerctl previous")),
    Key([], "XF86AudioStop", lazy.spawn("playerctl stop")),
    #    Key([], "XF86AudioPlay", lazy.spawn("mpc toggle")),
    #    Key([], "XF86AudioNext", lazy.spawn("mpc next")),
    #    Key([], "XF86AudioPrev", lazy.spawn("mpc prev")),
    #    Key([], "XF86AudioStop", lazy.spawn("mpc stop")),
    # QTILE LAYOUT KEYS
    Key([mod], "n", lazy.layout.normalize()),
    Key([mod], "space", lazy.next_layout()),
    # CHANGE FOCUS
    Key([mod], "Up", lazy.layout.up()),
    Key([mod], "Down", lazy.layout.down()),
    Key([mod], "Left", lazy.layout.left()),
    Key([mod], "Right", lazy.layout.right()),
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),
    # RESIZE UP, DOWN, LEFT, RIGHT
    Key(
        [mod, "control"],
        "l",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
    ),
    Key(
        [mod, "control"],
        "Right",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
    ),
    Key(
        [mod, "control"],
        "h",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
    ),
    Key(
        [mod, "control"],
        "Left",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
    ),
    Key(
        [mod, "control"],
        "k",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
    ),
    Key(
        [mod, "control"],
        "Up",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
    ),
    Key(
        [mod, "control"],
        "j",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
    ),
    Key(
        [mod, "control"],
        "Down",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
    ),
]

group_names = [
    ("TER", {"layout": "monadtall"}),
    ("WWW", {"layout": "monadtall"}),
    ("DEV", {"layout": "monadtall"}),
    ("SQL", {"layout": "monadtall"}),
    ("API", {"layout": "monadtall"}),
    ("SYS", {"layout": "monadtall"}),
    ("DOC", {"layout": "monadtall"}),
    ("CHAT", {"layout": "monadtall"}),
    ("MUS", {"layout": "monadtall"}),
]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(
        Key([mod], str(i), lazy.group[name].toscreen())
    )  # Switch to another group
    keys.append(
        Key([mod, "shift"], str(i), lazy.window.togroup(name))
    )  # Send current window to another group

layout_theme = {
    "border_width": 2,
    "margin": 6,
    "border_focus": "0a6c8b",
    "border_normal": "1D2330",
}

layouts = [
    layout.MonadTall(**layout_theme),
    layout.Max(**layout_theme),
    layout.Tile(shift_windows=True, **layout_theme),
    layout.Stack(num_stacks=2),
    layout.TreeTab(
        font="Share Tech Mono",
        fontsize=14,
        sections=["FIRST", "SECOND"],
        section_fontsize=11,
        bg_color="141414",
        active_bg="90C435",
        active_fg="000000",
        inactive_bg="384323",
        inactive_fg="a0a0a0",
        padding_y=5,
        section_top=10,
        panel_width=320,
    ),
    layout.Floating(**layout_theme),
]

colors = [
    ["#0f1219", "#0f1219"],  # panel background
    ["#434758", "#434758"],  # background for current screen tab
    ["#ffffff", "#ffffff"],  # font color for group names
    ["#a3563d", "#a3563d"],  # border line color for current tab
    ["#468b9d", "#468b9d"],  # color for clock
    ["#567788", "#567788"],  # color for battery
    ["#0a6c8b", "#0a6c8b"],  # color for volume
    ["#b79862", "#b79862"],  # color for updates
    ["#a3563d", "#a3563d"],  # color for memory
    ["#5b5655", "#5b5655"],  # color for cpu
    ["#468b9d", "#468b9d"],
]  # window name

prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

##### DEFAULT WIDGET SETTINGS #####
widget_defaults = dict(
    font="Share Tech Mono", fontsize=14, padding=2, background=colors[6]
)
extension_defaults = widget_defaults.copy()

# WIDGETS FOR THE BAR


def init_widgets_list():
    widgets_list = [
        widget.Sep(linewidth=0, padding=6, foreground=colors[2], background=colors[0]),
        widget.Image(
            filename="~/.config/qtile/icons/arco.png",
            background=colors[0],
            mouse_callbacks={"Button1": lambda qtile: qtile.cmd_spawn("dmenu_run")},
        ),
        widget.Sep(linewidth=0, padding=6, foreground=colors[2], background=colors[0]),
        widget.GroupBox(
            font="Share Tech Mono",
            fontsize=14,
            margin_y=3,
            margin_x=0,
            padding_y=5,
            padding_x=3,
            borderwidth=3,
            active=colors[2],
            inactive=colors[2],
            rounded=False,
            highlight_color=colors[1],
            highlight_method="line",
            this_current_screen_border=colors[3],
            this_screen_border=colors[4],
            other_current_screen_border=colors[0],
            other_screen_border=colors[0],
            foreground=colors[2],
            background=colors[0],
        ),
        widget.Prompt(
            prompt=prompt,
            font="Share Tech Mono",
            padding=10,
            foreground=colors[3],
            background=colors[1],
        ),
        widget.Sep(linewidth=0, padding=40, foreground=colors[2], background=colors[0]),
        widget.WindowName(foreground=colors[6], background=colors[0], padding=0),
        widget.TextBox(
            text="", background=colors[0], foreground=colors[7], padding=0, fontsize=37
        ),
        widget.TextBox(
            text=" ",
            padding=2,
            foreground=colors[2],
            background=colors[7],
            fontsize=10,
        ),
        widget.CPU(
            foreground=colors[2],
            background=colors[7],
            mouse_callbacks={
                "Button1": lambda qtile: qtile.cmd_spawn(myTerm + " -e htop")
            },
            format="CPU {load_percent}%",
            padding=5,
        ),
        widget.TextBox(
            text="", background=colors[7], foreground=colors[8], padding=0, fontsize=37
        ),
        widget.TextBox(
            text=" ",
            padding=2,
            foreground=colors[2],
            background=colors[8],
            fontsize=10,
        ),
        widget.Memory(
            foreground=colors[2],
            background=colors[8],
            mouse_callbacks={
                "Button1": lambda qtile: qtile.cmd_spawn(myTerm + " -e htop")
            },
            padding=5,
        ),
        # widget.Battery(
        #         discharge_char='↓',
        #         charge_char='↑',
        #         format='{char} {percent:2.0%}',
        #         foreground = colors[2],
        #         background = colors[6],
        #         padding = 5
        #         ),
        widget.TextBox(
            text="", background=colors[8], foreground=colors[5], padding=0, fontsize=37
        ),
        widget.Clock(
            foreground=colors[2], background=colors[5], format="%A, %B %d  [ %H:%M ]"
        ),
        widget.TextBox(
            text="", background=colors[5], foreground=colors[6], padding=0, fontsize=37
        ),
        widget.Systray(foreground=colors[0], background=colors[6], padding=5),
        widget.Sep(linewidth=0, padding=10, foreground=colors[0], background=colors[6]),
    ]
    return widgets_list


def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1  # Slicing removes unwanted widgets on Monitors 1,3


def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    return widgets_screen2  # Monitor 2 will display all widgets in widgets_list


def init_screens():
    return [
        Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=1.0, size=20)),
        Screen(top=bar.Bar(widgets=init_widgets_screen2(), opacity=1.0, size=20)),
        Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=1.0, size=20)),
    ]


if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()


# MOUSE CONFIGURATION
mouse = [
    Drag(
        [mod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()
    ),
]

dgroups_key_binder = None
dgroups_app_rules = []

# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME
# BEGIN


#@hook.subscribe.client_new
#def assign_app_group(client):
#    d = {}
    #     #########################################################
    #     ################ assgin apps to groups ##################
    #     #########################################################
#    d["WWW"] = [
#        "Qutebrowser",
#        "Firefox",
#        "Brave",
#        "Brave-browser",
#        "qutebrowser",
#        "firefox",
#        "brave",
#        "brave-browser",
#   ]
    #     d["2"] = [ "Atom", "Subl3", "Geany", "Brackets", "Code-oss", "Code", "TelegramDesktop", "Discord",
    #                "atom", "subl3", "geany", "brackets", "code-oss", "code", "telegramDesktop", "discord", ]
    #     d["3"] = ["Inkscape", "Nomacs", "Ristretto", "Nitrogen", "Feh",
    #               "inkscape", "nomacs", "ristretto", "nitrogen", "feh", ]
    #     d["4"] = ["Gimp", "gimp" ]
    #     d["5"] = ["Meld", "meld", "org.gnome.meld" "org.gnome.Meld" ]
    #     d["6"] = ["Vlc","vlc", "Mpv", "mpv" ]
    #     d["7"] = ["VirtualBox Manager", "VirtualBox Machine", "Vmplayer",
    #               "virtualbox manager", "virtualbox machine", "vmplayer", ]
    #     d["8"] = ["Thunar", "Nemo", "Caja", "Nautilus", "org.gnome.Nautilus", "Pcmanfm", "Pcmanfm-qt",
    #               "thunar", "nemo", "caja", "nautilus", "org.gnome.nautilus", "pcmanfm", "pcmanfm-qt", ]
    #     d["9"] = ["Evolution", "Geary", "Mail", "Thunderbird",
    #               "evolution", "geary", "mail", "thunderbird" ]
    #     d["0"] = ["Spotify", "Pragha", "Clementine", "Deadbeef", "Audacious",
    #               "spotify", "pragha", "clementine", "deadbeef", "audacious" ]
    #     ##########################################################
#    wm_class = client.window.get_wm_class()[0]

#    for i in range(len(d)):
#        if wm_class in list(d.values())[i]:
#            group = list(d.keys())[i]
#            client.togroup(group)
#            client.group.cmd_toscreen()


# END
# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME


main = None


@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser("~")
    subprocess.call([home + "/.config/qtile/scripts/autostart.sh"])


@hook.subscribe.startup
def start_always():
    # Set the cursor to something sane in X
    subprocess.Popen(["xsetroot", "-cursor_name", "left_ptr"])


@hook.subscribe.client_new
def set_floating(window):
    if (
        window.window.get_wm_transient_for()
        or window.window.get_wm_type() in floating_types
    ):
        window.floating = True


floating_types = ["notification", "toolbar", "splash", "dialog"]


follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        {"wmclass": "Arcolinux-welcome-app.py"},
        {"wmclass": "Arcolinux-tweak-tool.py"},
        {"wmclass": "confirm"},
        {"wmclass": "dialog"},
        {"wmclass": "download"},
        {"wmclass": "error"},
        {"wmclass": "file_progress"},
        {"wmclass": "notification"},
        {"wmclass": "splash"},
        {"wmclass": "toolbar"},
        {"wmclass": "confirmreset"},
        {"wmclass": "makebranch"},
        {"wmclass": "maketag"},
        {"wmclass": "Arandr"},
        {"wmclass": "feh"},
        {"wmclass": "Galculator"},
        {"wmclass": "arcolinux-logout"},
        {"wmclass": "xfce4-terminal"},
        {"wname": "branchdialog"},
        {"wname": "Open File"},
        {"wname": "pinentry"},
        {"wmclass": "ssh-askpass"},
    ],
    fullscreen_border_width=0,
    border_width=0,
)
auto_fullscreen = True

focus_on_window_activation = "focus"  # or smart

wmname = "QTile"
